﻿using System;
using System.Collections.Generic;

namespace Tennis
{
    public class TennisGame
    {

        private Dictionary<int, string> scoreMap = new Dictionary<int, string>()
        {
            {0, "Love"},
            {1, "Fifteen"},
            {2, "Thirty"},
            {3, "Fourty"}
        };

        private int playerOneScore;
        private int playerTwoScore;


        public TennisGame()
        {
        }

        public string GetScore()
        {
            if(playerOneScore == playerTwoScore)
            {
                return EqualityScore();
            }
            else
            {
                return NotEqualityScore();
            }
        }

        public void ScorePlayerOne()
        {
            playerOneScore++;
        }

        public void ScorePlayerOnePlusX(int nbOfIncrement)
        {
            for(int i =  0; i < nbOfIncrement; i++)
            {
                playerOneScore++;
            }
        }

        public void ScorePlayerTwo()
        {
            playerTwoScore++;
        }

        public void ScorePlayerTwoPlusX(int nbOfIncrement)
        {
            for (int i = 0; i < nbOfIncrement; i++)
            {
                playerTwoScore++;
            }
        }

        private string EqualityScore()
        {
            if(playerOneScore < 4)
            {
                return $"{scoreMap[playerOneScore]}-{scoreMap[playerTwoScore]}";
            }
            return $"{scoreMap[3]}-{scoreMap[3]}";
        }

        private string NotEqualityScore()
        {
            if (Math.Max(playerOneScore, playerTwoScore) <= 3)
            {
                return $"{scoreMap[playerOneScore]}-{scoreMap[playerTwoScore]}";
            }
            else
            {
                return AdvantageOrWin();
            }
        }

        private string AdvantageOrWin()
        {
            int differenceOfPoints = Math.Abs(playerOneScore - playerTwoScore);
            if (differenceOfPoints == 1)
            {
                return AdvantageScore();
            }
            else
            {
                return Won();
            }
        }

        private string AdvantageScore()
        {
            if (playerOneScore > playerTwoScore)
            {
                return "Advantage player one";
            }
            return "Advantage player two";
        }

        private string Won()
        {
            if (playerOneScore > playerTwoScore)
            {
                return "Win player one";
            }
            return "Win player two";
        }
    }
}
