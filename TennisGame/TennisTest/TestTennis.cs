﻿using System;
using Tennis;
using Xunit;

namespace TennisTest
{
    public class TestTennis
    {
        TennisGame tennisGame = new TennisGame();

        [Theory]
        [InlineData("Love-Love", 0, 0)]
        [InlineData("Fifteen-Fifteen", 1, 1)]
        [InlineData("Thirty-Thirty", 2, 2)]
        [InlineData("Fourty-Fourty", 3, 3)]
        [InlineData("Advantage player one", 4, 3)]
        [InlineData("Advantage player two", 4, 5)]
        [InlineData("Win player two", 7, 9)]
        [InlineData("Fifteen-Fourty", 1, 3)]
        [InlineData("Love-Fourty", 0, 3)]
        [InlineData("Fourty-Thirty", 3, 2)]

        public void GetScoreTest(string expectedValue, int numberServiceWonPlayer1, int numberServiceWonPlayer2)
        {
            tennisGame.ScorePlayerOnePlusX(numberServiceWonPlayer1);
            tennisGame.ScorePlayerTwoPlusX(numberServiceWonPlayer2);
            Assert.Equal(expectedValue, tennisGame.GetScore());
        }
    }
}
