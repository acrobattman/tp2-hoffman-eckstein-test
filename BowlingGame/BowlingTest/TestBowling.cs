﻿using System;
using Bowling;
using Xunit;

namespace BowlingTest
{
    public class TestBowling
    {
        GameBowling bowlingGame = new GameBowling();

        /*[Fact]
        public void ThrowInGutter()
        {
            for(int i = 0; i < 20; i++)
            {
                bowlingGame.Throw(0);
            }

            Assert.Equal(0, bowlingGame.Score());
        }*/

        [Fact]
        public void ThrowInGutter()
        {
            ThrowManyPins(20, 0);
            Assert.Equal(0, bowlingGame.Score());
        }


        /*[Fact]
        public void ThrowInOnlyOneAll()
        {
            for (int i = 0; i < 20; i++)
            {
                bowlingGame.Throw(1);
            }

            Assert.Equal(20, bowlingGame.Score());
        }*/

        [Fact]
        public void ThrowInOnlyOneAll()
        {
            ThrowManyPins(20, 1);

            Assert.Equal(20, bowlingGame.Score());
        }

        [Fact]
        public void ThrowOneSpare()
        {
            ThrowSpare();

            bowlingGame.Throw(7);
            ThrowManyPins(17, 0);
            Assert.Equal(24, bowlingGame.Score());
        }

        [Fact]
        public void ThrowOneStrike()
        {
            ThrowStrike();

            bowlingGame.Throw(4);
            bowlingGame.Throw(5);
            ThrowManyPins(16, 0);
            Assert.Equal(28, bowlingGame.Score());
        }

        [Fact]
        public void PerfectGame()
        {
            ThrowManyPins(12, 10);
            Assert.Equal(300, bowlingGame.Score());
        }


        private void ThrowManyPins(int n, int pins)
        {
            for(int i = 0; i < n; i++)
            {
                bowlingGame.Throw(pins);
            }
        }

        private void ThrowSpare()
        {
            bowlingGame.Throw(4);
            bowlingGame.Throw(6);
        }

        private void ThrowStrike()
        {
            bowlingGame.Throw(10);
        }
    }
}
