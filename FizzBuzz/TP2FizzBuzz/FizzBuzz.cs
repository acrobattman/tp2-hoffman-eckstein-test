﻿using System;
namespace TP2FizzBuzz
{
    public class FizzBuzz
    {

        public FizzBuzz()
        {
        }

        public string Generate(int minNumber, int maxNumber)
        {
            return EvaluateNumbers(minNumber, maxNumber);
        }

        private string EvaluateNumbers(int minNumber, int maxNumber)
        {
            string result = "";

            while (minNumber <= maxNumber)
            {
                result += EvaluateNumber(minNumber++);
            }

            return result;
        }


        private string EvaluateNumber(int number)
        {
            if (number % 15 == 0)
            {
                return "FizzBuzz";
            }
            if (number % 3 == 0)
            {
                return "Fizz";
            }
            if (number % 5 == 0)
            {
                return "Buzz";
            }
            return number.ToString();
        }
    }
}
